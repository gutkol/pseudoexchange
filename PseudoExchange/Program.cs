﻿using PseudoExchange.Classes;
using PseudoExchange.Classes.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PseudoExchange
{
    class Program
    {
        static readonly Suite suite = new Suite();

        static async Task Main(string[] args)
        {
            int choiseMenu = 0;
            List<StockOffer.offer> offers = new List<StockOffer.offer>();

            TextMenu();

            RandomGenerator generator = new RandomGenerator();
            generator.GenerateList();

            do
            {
                try
                {
                    choiseMenu = Convert.ToInt32(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("\nNiepoprawny wybór menu!");
                    Console.WriteLine("Proszę wybrać prawidłową opcję.");
                    choiseMenu = -1;
                }

                switch (choiseMenu)
                {
                    case (int)Menu.History:
                        Console.Clear();

                        int historyChoise = 0;

                        do
                        {
                            try
                            {
                                Console.WriteLine();
                                TextMenuHistory();

                                historyChoise = Convert.ToInt32(Console.ReadLine());
                            }
                            catch (FormatException)
                            {
                                Console.WriteLine("Nieprawidłowe dane. Dane muszą być liczbą od 1 do n");
                            }

                            switch (historyChoise)
                            {
                                case 1:
                                    offers = suite.History();

                                    Console.Clear();
                                    Console.WriteLine("Id \t\t\t Firma \t\t\t Wartość\n");

                                    foreach (var offer in offers)
                                    {
                                        Console.WriteLine("{0} \t\t\t {1} \t\t\t {2}", offer.Id, offer.Instrument, offer.Value);
                                    }

                                    break;
                                case 2:

                                    try
                                    {
                                        Console.WriteLine("Proszę podać liczbę od");
                                        int from = Convert.ToInt32(Console.ReadLine());

                                        Console.WriteLine("Proszę podać liczbę do");
                                        int to = Convert.ToInt32(Console.ReadLine());

                                        offers = suite.History(from, to);

                                        Console.Clear();
                                        Console.WriteLine("\nId \t\t\t Firma \t\t\t Wartość");

                                        foreach (var offer in offers)
                                        {
                                            Console.WriteLine("\n{0} \t\t\t {1} \t\t\t {2}", offer.Id, offer.Instrument, offer.Value);
                                        }
                                    }
                                    catch (FormatException)
                                    {
                                        Console.WriteLine("Podano nieprawidłowe dane");
                                    }

                                    break;

                                case 0:
                                    TextMenu();
                                    break;
                            }
                        } while (historyChoise != 0);

                        break;

                    case (int)Menu.Watcher:
                        Console.Clear();

                        Console.WriteLine("Proszę wybrać jedną z firm, którą chcesz zakupić:");

                        for (int i = 0; i < generator.Companies.Length; i++)
                        {
                            Console.WriteLine("{0}. {1}", (i + 1), generator.Companies[i]);
                        }

                        int companyChoise = 0;
                        int companyValue = 0;

                        do
                        {
                            try
                            {
                                companyChoise = Convert.ToInt32(Console.ReadLine());
                            }
                            catch (FormatException)
                            {
                                Console.WriteLine("Wybrano zły numer firmy!");
                                Console.WriteLine("Proszę wybrać właściwy numer firmy!");

                                companyChoise = -1;
                            }
                        } while (!(companyChoise - 1 >= 0 && companyChoise - 1 <= generator.Companies.Length));

                        do
                        {
                            try
                            {
                                Console.WriteLine("Proszę podać wartość wartość. (Wartość musi być większa niż 1)");

                                companyValue = Convert.ToInt32(Console.ReadLine());
                            }
                            catch (FormatException)
                            {
                                Console.WriteLine("Podano nieprawidłową wartość, wartość musi być liczbą.");
                            }
                        } while (!(companyValue >= 1));

                        if (companyChoise - 1 >= 0 && companyChoise - 1 <= generator.Companies.Length && companyValue >= 1)
                        {
                            suite.Watcher(generator.Companies[companyChoise - 1], companyValue);
                            TextMenu();
                        }

                        break;

                    case (int)Menu.Transactions:
                        Console.Clear();

                        suite.Transactions();

                        Console.WriteLine("\nKliknij dowolny przycisk aby powrócić.");
                        Console.ReadKey();

                        TextMenu();
                        break;

                    case (int)Menu.Run:
                        Console.Clear();

                        Watcher buyer1 = new Watcher("Ja");
                        await suite.Run(buyer1);

                        TextMenu();
                        break;
                }
            } while (choiseMenu != 0);
        }

        static void TextMenu()
        {
            Console.Clear();

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\t\t\t\t\t ----MENU---- \n");
            Console.ResetColor();

            Console.WriteLine("Proszę wybrać opcję od 1-4 wpisując odpowiedni numer");
            Console.WriteLine("1. Pobierz historię ofert.");
            Console.WriteLine("2. Kup ofertę.");
            Console.WriteLine("3. Moje zakupione oferty.");
            Console.WriteLine("4. Włącz giełdę.");
            Console.WriteLine("0. Wyjście.");
        }

        static void TextMenuHistory()
        {
            Console.WriteLine("Proszę wybrać jedną z opcji:\n");
            Console.WriteLine("1. Pobierz wszystkie dane.");
            Console.WriteLine("2. Pobierz dane z zakresu [od, do]");
            Console.WriteLine("0. Powrót");
        }

        enum Menu
        {
            History = 1,
            Watcher,
            Transactions,
            Run
        }
    }
}
