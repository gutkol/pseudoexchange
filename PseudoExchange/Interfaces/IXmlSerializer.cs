﻿using PseudoExchange.Classes.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PseudoExchange.Interfaces
{
    interface IXmlSerializer
    {
        void Serialize<T>(List<T> offers, bool serializeOffers) where T : class;
    }
}
