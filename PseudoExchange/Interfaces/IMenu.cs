﻿using PseudoExchange.Classes.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PseudoExchange.Interfaces
{
    interface IMenu
    {
        List<StockOffer.offer> History();

        List<StockOffer.offer> History(int from, int to);

        void Watcher(string companyName, int toValue);

        void Transactions();

        Task Run(Watcher buyer);
    }
}
