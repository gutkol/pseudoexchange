﻿using PseudoExchange.Classes.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PseudoExchange.Interfaces
{
    interface IXmlDeserializer
    {
        List<T> Deserialize<T>() where T : class;
    }
}
