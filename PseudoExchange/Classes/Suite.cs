﻿using PseudoExchange.Classes.Model;
using PseudoExchange.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace PseudoExchange.Classes
{
    class Suite : IMenu, IXmlDeserializer, IXmlSerializer
    {
        const string pathOferts = "oferts.xml";
        const string pathTransactions = "transactions.xml";

        object[] valuesToBuy = null;
        List<StockTransaction.Transaction> offersBought = new List<StockTransaction.Transaction>();

        public List<StockOffer.offer> History()
        {
            List<StockOffer.offer> offers = Deserialize<StockOffer.offer>();

            return offers.OrderByDescending(x => x.Id).ToList();
        }

        public List<StockOffer.offer> History(int from, int to)
        {
            List<StockOffer.offer> offers = Deserialize<StockOffer.offer>();

            return offers.Where(x => (int)x.Id >= from && (int)x.Id <= to).OrderByDescending(x => x.Id).ToList();
        }

        public async Task Run(Watcher buyer)
        {
            List<Watcher> bots = new List<Watcher>();

            for (int i = 1; i <= 2; i++)
            {
                bots.Add(new Watcher("Buyer" + i));
                await bots.ElementAt(i - 1).GenerateBotChoisen("PZU");
            }

            StockPlayer stockPlayers = new StockPlayer(bots);
            List<StockOffer.offer> allOffers = Deserialize<StockOffer.offer>();

            offersBought = new List<StockTransaction.Transaction>();

            foreach(var offer in allOffers)
            {
                await stockPlayers.ProcessAsync(offer);
                await Task.Delay(500);
            }

            if (valuesToBuy != null)
            {
                List<StockOffer.offer> offersChoise = allOffers.Where(x => (string)x.Instrument == (string)valuesToBuy[0] && (int)x.Value < (int)valuesToBuy[1]).ToList();

                foreach(var offer in offersChoise)
                {
                    offersBought.Add(new StockTransaction.Transaction() {
                        Id = offer.Id,
                        Watcher = buyer.BuyerName,
                        Instrument = offer.Instrument,
                        Value = offer.Value
                    });
                }

                List<StockTransaction.Transaction> allTransactions = Deserialize<StockTransaction.Transaction>();

                IEnumerable<StockTransaction.Transaction> trans = offersBought.Except(allTransactions.Where(x => x.Watcher.ToString() == buyer.BuyerName));

                Serialize(trans.ToList(), false);
            }
        }

        public void Transactions()
        {
            List<StockTransaction.Transaction> transactions = Deserialize<StockTransaction.Transaction>();

            foreach(var transaction in transactions)
            {
                Console.Write("{0} \t\t\t\t {1} \t\t\t\t {2} \t\t\t\t {3}\n", transaction.Id, transaction.Watcher ,transaction.Instrument, transaction.Value);
            }
        }

        public void Watcher(string companyName, int toValue)
        {
            valuesToBuy = new object[]{ companyName, toValue };
        }

        public List<T> Deserialize<T>() where T : class
        {
            List<T> offers = new List<T>();
            XmlSerializer deserializer = new XmlSerializer(typeof(List<T>));

            if (offers is List<StockOffer.offer>)
            {
                using (var reader = XmlReader.Create(pathOferts))
                {
                    offers = (List<T>)deserializer.Deserialize(reader);
                }
            }

            if (offers is List<StockTransaction.Transaction>)
            {
                if (IsXmlExist(false))
                {
                    using (var reader = XmlReader.Create(pathTransactions))
                    {
                        offers = (List<T>)deserializer.Deserialize(reader);
                    }
                }
            }

            return offers;
        }

        public void Serialize<T>(List<T> offers, bool serializeOffers) where T : class
        {
            XmlWriterSettings settings = new XmlWriterSettings()
            {
                OmitXmlDeclaration = true,
                Indent = true
            };

            XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";

            if (!IsXmlExist(serializeOffers))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<T>));

                using (XmlWriter writer = XmlWriter.Create(serializeOffers ? pathOferts : pathTransactions, settings))
                {
                    serializer.Serialize(writer, offers);
                }
            }
            else
            {
                XDocument xDocument = XDocument.Load(serializeOffers ? pathOferts : pathTransactions);
                XElement root = xDocument.Element(serializeOffers ? "ArrayOfOffer" : "ArrayOfTransaction");
                IEnumerable<XElement> rows = root.Descendants(serializeOffers ? "offer" : "Transaction");
                XElement lastRow = rows.Last();

                if (offers is List<StockOffer.offer> castOffers)
                {
                    foreach (var offer in castOffers.OrderByDescending(x => x.Id))
                    {
                        lastRow.AddAfterSelf(new XElement("offer",
                        new XElement("Id", offer.Id, new XAttribute(xsi + "type", "xsd:int")),
                        new XElement("Instrument", offer.Instrument, new XAttribute(xsi + "type", "xsd:string")),
                        new XElement("Value", offer.Value, new XAttribute(xsi + "type", "xsd:int"))));
                    }

                    xDocument.Save(pathOferts);
                }

                if (offers is List<StockTransaction.Transaction> castTrans)
                {
                    foreach (var offer in castTrans.OrderByDescending(x => x.Id))
                    {
                        lastRow.AddAfterSelf(new XElement("Transaction",
                            new XElement("Watcher", "", new XAttribute(xsi + "type", "xsd:string")),
                            new XElement("Id", offer.Id, new XAttribute(xsi + "type", "xsd:int")),
                            new XElement("Instrument", offer.Instrument, new XAttribute(xsi + "type", "xsd:string")),
                            new XElement("Value", offer.Value, new XAttribute(xsi + "type", "xsd:int"))));
                    }

                    xDocument.Save(pathTransactions);
                }
            }
        }

        public bool IsXmlExist(bool isOffers)
        {
            if (isOffers)
                return File.Exists(pathOferts);
            else
                return File.Exists(pathTransactions);
        }
    }
}
