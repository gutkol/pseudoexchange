﻿using PseudoExchange.Classes.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PseudoExchange.Classes
{
    class StockPlayer
    {
        object locker;
        bool isBought = false;
        List<Task> tasks = null;
        List<Watcher> watchers = new List<Watcher>();

        public StockPlayer(List<Watcher> watchers)
        {
            this.watchers = watchers;
            locker = new object();
        }

        public async Task ProcessAsync(StockOffer.offer stockOffer)
        {
            tasks = new List<Task>();
            isBought = false;

            Action<Watcher> TryBuy = (Watcher watcher) =>
            {
                lock (locker)
                {
                    if (!isBought)
                    {
                        if (stockOffer.Instrument.ToString() == watcher.Instrument)
                        {
                            if ((int)stockOffer.Value < watcher.Value)
                            {
                                stockOffer.Buyer = watcher.BuyerName;
                                isBought = true;
                            }
                        }
                    }
                }
            };

            foreach (var watcher in watchers)
            {
                tasks.Add(new Task(() => TryBuy(watcher)));
            }

            Parallel.ForEach(tasks, x => x.Start());
            await Task.WhenAll(tasks);

            if (isBought)
            {
                Console.WriteLine("User: {0}, zakupił: {1} {2}", stockOffer.Buyer, stockOffer.Instrument, stockOffer.Value);
            }
            else
            {
                Console.WriteLine("{0} {1}",  stockOffer.Instrument, stockOffer.Value);
            }
        }
    }
}
