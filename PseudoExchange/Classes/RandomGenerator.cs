﻿using PseudoExchange.Classes.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace PseudoExchange.Classes
{
    class RandomGenerator
    {
        const int numberTogenerate = 100;
        public static readonly string[] companies;

        static RandomGenerator()
        {
            StreamReader readInstruments = new StreamReader("instruments.txt");
            companies = readInstruments.ReadToEnd().Split(new string[] { "\r\n"}, StringSplitOptions.RemoveEmptyEntries);
        }

        public string[] Companies
        {
            get
            {
                return companies;
            }
        }

        public void GenerateList()
        {
            Suite suite = new Suite();
            List<StockOffer.offer> offers = new List<StockOffer.offer>();

            if (suite.IsXmlExist(true))
            {
                int lastId = (int)suite.Deserialize<StockOffer.offer>().LastOrDefault().Id;

                offers = NewOffers(lastId);

                suite.Serialize(offers, true);
            }
            else
            {
                ////XmlSerializer serializer = new XmlSerializer(typeof(Offer.offers));

                //using (XmlWriter writer = XmlWriter.Create("oferts.xml", settings))
                //{
                //    foreach (var item in offers)
                //    {
                //        writer.WriteStartElement("offer");
                //        writer.WriteElementString("Id", item.Id.ToString());
                //        writer.WriteElementString("Instrument", item.Instrument.ToString());
                //        writer.WriteElementString("Value", item.Value.ToString());
                //        writer.WriteEndElement();
                //    }

                //    writer.Flush();
                //}


                offers = NewOffers(0);

                suite.Serialize(offers, true);
            }
        }

        private List<StockOffer.offer> NewOffers(int from)
        {
            Random random = new Random();
            List<StockOffer.offer> offers = new List<StockOffer.offer>();

            for (int i = from + 1; i <= from + numberTogenerate; i++)
            {
                offers.Add(new StockOffer.offer()
                {
                    Id = i,
                    Instrument = companies.OrderBy(x => Guid.NewGuid()).FirstOrDefault(),
                    Value = random.Next(10, 100)
                });
            }

            return offers;
        }

        static public int Miliseconds(int min, int max)
        {
            Random _random = new Random((int)DateTime.Now.Ticks);

            return _random.Next(min, max);
        }
    }
}
