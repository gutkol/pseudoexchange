﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PseudoExchange.Classes.Model
{
    class Watcher
    {
        string _buyerName;
        string _instrument;
        int _value;

        public string BuyerName {
            get
            {
                return _buyerName;
            }
        }

        public string Instrument
        {
            get
            {
                return _instrument;
            }
        }

        public int Value
        {
            get
            {
                return _value;
            }
        }

        public Watcher(string name)
        {
            _buyerName = name.Substring(0, 1).ToUpper() + name.Substring(1);
        }

        public async Task GenerateBotChoisen(string instrument)
        {
            Random random = new Random();
            await Task.Delay(RandomGenerator.Miliseconds(100, 200));

            _instrument = instrument;
            _value = random.Next(1, 100);
        }
    }
}
