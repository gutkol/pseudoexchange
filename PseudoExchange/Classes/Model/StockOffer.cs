﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PseudoExchange.Classes.Model
{
    public class StockOffer
    {

        // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class offers
        {

            private offer offerField;

            /// <remarks/>
            public offer offer
            {
                get
                {
                    return this.offerField;
                }
                set
                {
                    this.offerField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class offer
        {

            private object idField;

            private object instrumentField;

            private object valueField;

            private object buyerField;

            /// <remarks/>
            public object Id
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }

            /// <remarks/>
            public object Instrument
            {
                get
                {
                    return this.instrumentField;
                }
                set
                {
                    this.instrumentField = value;
                }
            }

            /// <remarks/>
            public object Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }

            public object Buyer
            {
                get
                {
                    return this.buyerField;
                }
                set
                {
                    this.buyerField = value;
                }
            }
        }


    }
}
