﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PseudoExchange.Classes.Model
{
    public class StockTransaction
    {

        // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class Transaction
        {

            private object idField;

            private object watcherField;

            private object instrumentField;

            private object valueField;

            /// <remarks/>
            public object Id
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
            public object Watcher
            {
                get
                {
                    return this.watcherField;
                }
                set
                {
                    this.watcherField = value;
                }
            }

            /// <remarks/>
            public object Instrument
            {
                get
                {
                    return this.instrumentField;
                }
                set
                {
                    this.instrumentField = value;
                }
            }

            /// <remarks/>
            public object Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }


    }
}
